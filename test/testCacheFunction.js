const cachedFunction = require('../cacheFunction');

function multiply(a, b) {
    return a * b;
}

const cacheFunction = cachedFunction(multiply);
console.log(cacheFunction(1,2));
console.log(cacheFunction(2,3));
console.log(cacheFunction(1,2));