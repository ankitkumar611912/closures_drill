const counterFactory = require('../counterFactory');
const counterObject = counterFactory();

console.log(counterObject.increment());
console.log(counterObject.increment());
console.log(counterObject.decrement());