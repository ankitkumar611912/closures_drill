function counterFactory() {
    let counter = 0;
    let counterObject =  {
        increment: function() {
            return ++counter; 
        },
        decrement: function() {
            return --counter; 
        }
    };
    return counterObject;
}

module.exports = counterFactory;
