function limitFunctionCallCount(cb, n) {
    let callCount = 0; 
    return function() {
        if (callCount < n) {
            callCount++;
            cb();
        }
        else{
            return null;
        }
    };
}

module.exports = limitFunctionCallCount;