function cacheFunction(cb) {
    const cache = {}; 
    return function(...args) {
        const key = JSON.stringify(...args);
        if (!cache[key]) {
            cache[key] = cb(...args);
        }
        else{
            console.log(`Cached Result is ${cache[key]}`)
        }
        return cache[key]; 
    };
}

module.exports = cacheFunction;